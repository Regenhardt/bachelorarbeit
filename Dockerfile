FROM tensorflow/tensorflow:2.15.0-gpu

RUN apt-get update && apt-get install -y software-properties-common
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

COPY . /app

WORKDIR /app/Code