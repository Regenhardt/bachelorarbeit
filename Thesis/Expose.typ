#import "template.typ": project

#show: project.with(
  kind: "Exposé",
  title: "Analyse von Pose Estimation Sequenzen mit Deep Learning zur Sturzerkennung",
  authors: (
    (name: "Marlon Regenhardt", 
    email: "marlon.regenhardt@haw-hamburg.de", 
    affiliation: "Hochschule für Angewandte Wissenschaften Hamburg", 
    postal: "", 
    phone: ""),
  ),
  language: "de"
)

= Hintergrund
Die alternde Bevölkerung wird das deutsche Pflegesystem in Zukunft auf eine harte Probe stellen. Im Jahr 2035 soll es "gut 4 Millionen" Pflegefälle in ganz Deutschland geben@kochskaemper2018pflegefallzahlen, jedoch sollen dann auch 307000 Pflegekräfte fehlen@statista2022pflegemangel. Dabei werden je nach Bundesland "40 bis 55 Prozent" der Pflegefälle bereits informell gepflegt, also von Angehörigen@kochskaemper2018pflegefallzahlen. Eine der Hauptgefahren für ältere Menschen ohne dauerhafte Betreuung sind Stürze, welche für 70% der verletzungsbedingten Todesfälle bei Menschen über 75 Jahre veranwortlich und eine der größten Ängste älterer, alleinlebender Menschen sind@ageuk2017falls. Laut WHO 2008 erleidet jede dritte Person zwischen 75 und 80 Jahren jahrlich mindestens einen Sturz, wovon 20-30% zu leichten oder schweren Verletzungen führen. Sie sind für 10-15% aller Zugänge in der Notaufnahme verantwortlich@WHO2008. Bei Personen ab dem 65. Lebensjahr sind Stürze "die siebthäufigste Todesursache". In den Niederlanden sind Tode aufgrund von Stürzen besonders bei Menschen ab 70 in den letzten 20 Jahren extrem angestiegen@cbs2024deaths. Eine große Ursache für schwere Verletzungen, Krankenhausaufnahmen, Verlegungen in die Langzeitpflege oder Tod ist dabei das auf dem Boden Liegen für eine lange Zeit (über eine Stunde@Fleminga2227 bzw. über 30min@cbs2024deaths).

= Problemstellung
Stürze im häuslichen Umfeld müssen in adäquater Zeit automatisch und zuverlässig erkannt werden, um rechtzeitig Hilfe holen zu können, während Aktionen des täglichen Lebens, wie z. B. Sachen aufheben und sich hinsetzen, nicht fälschlicherweise auch als Sturz gemeldet werden sollen, um unnötige Arbeit zu vermeiden. Am Körper angebrachte Sensoren können vergessen oder aus Unwille abgelegt werden und selbst mit der steigenden Verbreitung von Smartwatches können diese liegengelassen werden oder ihren Akku unbemerkt aufbrauchen. Eine Erkennung muss also mit rein passiven Sensoren funktionieren, die nicht am Körper der Person befestigt sind, um den einfachen Einsatz zu ermöglichen.

= Lösungsansatz
Primär soll ein automatisiertes System zur Erkennung von Stürzen älterer Menschen in häuslicher Umgebung entwickelt und implementiert werden, das zur Sicherheit und dem Wohlbefinden dieser Menschen beitragen kann. Die Erkennung soll über Reihen von RGB-Kameras aufgenommenen Bildern geschehen um eine dauerhafte Überwachung zu ermöglichen. Ein wichtiger Bestandteil des Systems ist das von Google entwickelte MediaPipe@mediapipe, das im ersten Schritt die Kamerabilder in 36-dimensionale Vektoren zur Repräsentation des Skeletts einer Person umrechnet. Zeitreihen dieser Vektoren sollen dann in ein neuronales Netz gespeist werden, das darauf trainiert ist, aus einem Bewegungsmuster einen potenziellen Sturz zu erkennen. Dieses Netz soll einen Convolutional Layer zur Extraktion von Features aus den von MediaPipe gelieferten Skelettdaten enthalten, gefolgt von einem oder mehreren LSTM-Layern zur Verarbeitung der Zeitreihen. Die Ausgabe des Netzes soll eine Wahrscheinlichkeit sein, dass es sich bei der gegebenen Bildfolge um einen Sturz handelt, anhand derer dann entschieden werden kann, ob eine Meldung über einen Sturz an nachgelagerte Systeme weitergegeben werden soll.

= Vorhandene Forschung
Die Sturzerkennung ist ein schon länger bekanntes Thema, somit gibt es bereits einiges an Forschung dazu, wobei das meiste nicht auf herkömmlichen RGB-Kameras beruht. Was dieser Arbeit nahekommt ist zum Beispiel Thermal Imaging Based Elderly Fall Detection@Thermal, was zwar auf Wärmebildern trainiert wurde, jedoch mit dem KUL Simulated Fall@KUL Dataset validiert wurde, welches RGB-Aufnahmen enthält.

Außerdem wurde die Sturzerkennung per Radarüberwachung untersucht. Dabei wurden Zeitreihen von Radarsignalen durch verschiedene Algorithmen analysiert@amin2016radar.

Zudem wurde an der HAW Hamburg bereits 2022 mit der Extraktion aus MediaPipe und Analyse von Skeletten gearbeitet, wobei hier im Endeffekt kein neuronales Netz zur Auswertung der Skelette zum Einsatz kam@schmidpeter2022. Hier wurde ebenfalls eine Alternative zu MediaPipe, OpenPose, in Betracht gezogen, das jedoch keine sinnvollen Vorteile gegenüber MediaPipe bieten konnte.

= Methodik
#figure(image("img/Pipeline.drawio.svg"), caption: [Pipeline zur Sturzerkennung])

*Vorverarbeitung der Bilder:* Zunächst wird mit einer vorhandenes Pose Estimaion Bibliothek aus den Bildern jeweils das Skelett der Person extrahiert. Dies liefert ein Set aus Koordinaten, der sogenannten KeyPoints einer Person, also von Körperteilen. Daraus wurden bei Schmidpeter 2022@schmidpeter2022 Winkel zur weiteren Auswertung berechnet. Hier soll jedoch direkt mit den gelieferten KeyPoints gearbeitet werden. #linebreak()
*Verarbeitung der Skelette:* Hierfür soll ein neuronales Netz benutzt werden das sequentielle Daten verarbeiten kann, also ein RNN, LSTM oder Transformer. Es muss abgewägt werden, wie groß das Netz tatsächlich sein darf, um immer noch in Echtzeit Bildsequenzen verarbeiten zu können. Transformer sind mächtig, aber auch dementsprechend rechenintensiv, sollten also nur verwendet werden, falls eine weniger aufwändige Architektur keine guten Ergebnisse liefert. Außerdem liefert MediaPipe sehr viele KeyPoints, von denen vermutlich nicht alle nötig sind um einen Sturz zu erkennen, zum Beispiel einzelne Finger. Um eine bessere Leistung zu erzielen, soll gegebenenfalls geprüft werden, inwiefern Dynamic Time Warping@3DDTW (DTW) genutzt werden kann, um eine grobe Auswahl zu treffen, ob ein gegebenes Bild überhaupt an das neuronale Netz weitergegeben werden soll. Hier muss jedoch mit einer Art Cache gearbeitet werden, um mehrere Bilder sowohl per DTW auszuwerten, als auch bei Bedarf die gesamte Bildfolge an das neuronale Netz zu geben. #linebreak()
*Werkzeuge:* OpenPose kann im Gegensatz zu MediaPipe auch mehrere Personen in einem Bild erkennen, was jedoch für diesen Anwendungsfall nicht nötig ist. Da MediaPipe laut Schmidpeter 2022@schmidpeter2022 gut handhabbare APIs anbietet, wird für diese Arbeit MediaPipe zur Vorverarbeitung genutzt. Es beitet jeweils eine API für C++, Python und Javascript. Zum Prototyping und Training der neuronalen Netze soll Python verwendet werden, da dies eine hohe Abstraktion ermöglicht und hier keine Höchstleistung nötig ist. Nach dem Training sollen Compiler wie TVM, OpenVINO oder DeepSparse verwendet werden, um die Inferenzzeit zu verringern. Für das neuronale Netz wird Tensorflow verwendet, da die APIs bekannt sind und MediaPipe auch auf TensorFlow basiert. Zum Training wird ein Docker-Image erstellt, das dann auf einem Server der HAW Hamburg genutzt wird. Für die Versionsverwaltung und der Bau des Docker-Images wird gitlab.com genutzt. #linebreak()
*Trainingsdaten:* Zum Training soll primär das KUL Simulated Fall Dataset@KUL verwendet werden, das zu genau diesem Zweck erstellt wurde. Es enthält Videos sowohl verschiedener Stürze als auch Aktionen des täglichen Lebens in einer Wohnung, somit stehen auch passende Negativbeispiele zur Verfügung. Die Videos und Metadaten sind frei verfügbar. Es stehen 55 Stürze aus je 5 Perspektiven zur Verfügung. #linebreak()
Als Sturz werden hierbei Zeitreihen genommen, deren Ende ein Sturz ist und deren Anfang 3 Sekunden davor liegt. Jeder Frame während des Sturzes wird als letzter Frame einer Zeitreihe genommen, die positiven Zeitreihen überlappen sich also. Als Negativbeispiele werden Zeitreihen genommen, die 3 Sekunden lang sind und keinen Sturz enthalten. Das sind einerseits Zeitreihen aus expliziten ADL-Videos aus dem KUL-Dataset, andererseits auch Zeitreihen aus den Sturzvideos, die vor dem Anfang des Sturzes enden und solche, die nach dem Ende des Sturzes anfangen (implizite ADL-Sequenzen). Somit sind sowohl viele unterschiedliche Negativbeispiele vorhanden, als auch genügend Beispiele von liegenden Personen die gerade nicht stürzen. #linebreak()
*Training:* Da nicht einschätzbar ist, welche Art der Trainingsdaten am besten funktionieren, sollen mehrere Modelle trainiert und verglichen werden: Ein Modell, das nur explizite ADL-Videos als Negativbeispiele nimmt, ein Modell, das nur implizite ADLs als Negativbeispiele nimmt und ein Modell, das beide Arten von Negativbeispielen nimmt. Außerdem ein Modell, das nur Negativbeispiele, also nur ADL-Sequenzen, erhält, was einer Anomaly Detection entspricht. #linebreak()
*Auswertung:* Zur anfänglichen Auswertung wird ein Model heruntergeladen und auf einem lokalen Rechner an einem Video aus dem KUL-Dataset getestet. Hierdurch soll eingeschätzt werden, ob das vorliegende Modell grundsätzlich funktioniert und ob es sich lohnt, diese Richtung weiter zu verfolgen. #linebreak()
Dann soll für jedes Modell eine Confusion Matrix erstellt werden, um die Modelle anhand aller Daten miteinander zu vergleichen. #linebreak()
*Auswahl:* Da in diesem Use Case False Negatives (also Stürze, die nicht erkannt werden) schlimmer sind als False Positives (also Aktionen des täglichen Lebens, die fälschlicherweise als Sturz erkannt werden), soll das Modell ausgewählt werden, das die wenigsten False Negatives hat, solange die False Positives nicht überhand nehmen.

= Ziele 
Das primäre Ziel dieser Arbeit ist es, die Zeit bis zum Eintreffen von Hilfe nach einem Sturz älterer Menschen zu minimieren. Durch die Entwicklung eines effizienten Sturzerkennungssystems soll gewährleistet werden, dass im Falle eines Sturzes schnellstmöglich Hilfe gerufen wird. Dies ist von entscheidender Bedeutung, da schnelle medizinische Intervention oft den Unterschied zwischen einer schnellen Genesung und langfristigen gesundheitlichen Problemen ausmachen kann.

Zusätzlich zu diesem Hauptziel soll das System auch dazu beitragen, älteren Menschen ein Gefühl der Sicherheit und Unabhängigkeit in ihrem eigenen Zuhause zu vermitteln. In einer Gesellschaft, in der die Anzahl älterer Menschen stetig zunimmt, ist es wichtig, Lösungen zu finden, die es ihnen ermöglichen, ohne ständige Angst vor Unfällen zu leben. Darüber hinaus soll das System dazu beitragen, die psychologische Angst vor dem Alleinsein zu lindern.

#pagebreak(weak: true)
#set page(header: [])
= Literatur

#bibliography("references.bib")
