MediaPipe provides two result sets: Camera-centric poses & world poses
- Camera-centric poses are the poses in the camera coordinate system, normalized to [0, 1] range
- World poses are the poses in the world coordinate system, normalized to [0, 1] range

Initial prototyping used the webcam to get to know MediaPipe and its result sets and overlay functions.  
As MediaPipe is trained on RGB images and cv2 uses BGR images, the input image has to be converted from BGR to RGB before being passed to MediaPipe.

To overlay on the input image, we use a function provided by MediaPipe that transforms the camera-centric poses to the image coordinate system.

To visualize other coordinates and possible transformations, we create additional functions and windows to display the results in sync with the overlayed input image.

After a basic understanding was established, the input was changed to a video file to continue prototyping on a more realistic input, one of the KUL videos (Fall2_Cam4.avi).  
This video contains a chair, which is not part of the pose estimation, but is detected as a person, as long as there is no actual person in the frame, in which case MediaPipe immediately switches to only detecting the person.

4 images are generated for initial prototyping:
- Input image with overlayed camera-centric motion trajectory (last 20 poses)
- Black background with overlayed world pose normalized to the image size
- Black background with overlayed world motion trajectory (last 20 poses) normalized and fitted to the image
- Black background with overlayed raw world pose

The normalized world pose omits the person's movement across the room and is therefore not usable for training an AI to detect a fall.

The fitted world motion trajectory does not work for this purpose, as each pose comes centered on the hips too, which causes the trajectory to be centered on the hips as well, completely omitting the person's movement across the room.

The fitted world pose was switched out for the fitted camera-centric pose, which is usable for training an AI to detect a fall as it includes the person's movement across the room.  
Opposed to using the raw camera-centric pose, the fitted pose is more robust to a person falling in different places in the image, therefore making it easier to train an AI and later use it on different videos.

The only better options would be to create a motion trajectory from the world pose modified to include the person's movement across the room, which would entail calculating the person's movement from the camera-centric poses and adding it to the world poses, which would be a lot of work and possibly still result in suboptimal results due to having to calculate where the ground is or having to rely on the hips being an indicator of the person's current position.

Using the camera-centric motion trajectory and fitting it to the image is the best option for this project.

To reduce the load on the neural network, provided poses are compared with the recent poses and only used if they are different enough. This way, the neural network is only used when the person is moving.

Fitting the motion trajectory to the image is done in two steps in order to have a separate function for usage by the neural network. This way the neural network can be trained on the fitted motion trajectory, while the overlayed image can be used for debugging and visualization.
 
KUL dataset has videos with a fall, but also videos with ADLs (Activities of Daily Living) like sitting down, standing up, picking up an object, etc. for training the AI to detect a fall and to not detect ADLs as a fall.

The metadata contain annotation for when a fall begins and ends. They do not contain annotations for when the ADLs begin and end.  
They have to be manually annotated to not have way more data for ADLs than for falls, or we use non-overlapping windows of a fixed size to create the ADL training data, which would be way less work, but also less accurate due to many resulting sequences would just be a person standing still.

Let's use the non-overlapping windows of a fixed size to create the ADL training data.  
Each window is the same size as the chosen window size for the fall detection: 45 for now, which is 3 seconds at 15 fps, as every other frame of the 30 fps videos is skipped to reduce the load on the neural network.  
The ADL videos are quite long compared to the fall videos, so there is way more ADL-data than fall data.  
To automatically choose which ADL sequences to use for training, the ADL sequences are sorted by how dynamic they are, which is calculated by the total distance between all the poses in the sequence. This way, the most dynamic sequences are chosen for training, which should provide the best non-fall data for training as the fall data is by definition very dynamic and therefore more similar to the most dynamic ADL-data.

There is a PoseProcessor class that handles the extraction of poses from sequentially passed images, providng normalized camera-centric sequences of poses.  
Using that is the VideoProcessor. It takes a video file, extracts sequences using the PoseProcessor and saves them to a file named after the video file. It can extract both fall sequences (one per frame close to the end label) and ADL sequences (non-overlapping windows of the same size as the fall sequences).  

There is also a function to sort the ADL sequences by how dynamic they are in order to later batch fall sequences with the most dynamic ADL sequences for training.  
-> This is not used for now, as the training data is shuffled before training anyway.

# Training

Training happens on the CSTI infrastructure. The servers have a file server for data storage mounted on /mnt/data/<name>.  
The code for training is in the repository, along with a Dockerfile that copies the code into a tensorflow image and installs the required packages.  
The gitlab CI pipeline builds the image and pushes it to the gitlab registry.

On the server there are a few script files in the home directory to pull, run and delete the image. These make sure the image is always up to date, the server is not cluttered with old images, and environment variables are properly passed to the container.   
Environment variables are used to pass the storage directory for both training data and the trained models.  
The training videos are downloaded from box.com via python script and saved in the storage directory.  
For training, the scripts request the training data from the Preprocessor, which runs mediapipe on a video if needed, or uses an already existing data file to provide the training data.  
The training scripts log messages about finished models or errors to stdout, which is redirected to a file in the storage directory.  
The trained models are saved to the storage directory.

## Small models training

To test different training scenarios, multiple training files are created. One for each of the following scenarios:
- Fall sequences with ADLs from the fall videos (train_without_explicit_adls)
- Fall sequences with ADLs from the fall videos and ADLs from the ADL videos (train_with_explicit_adls)
- Fall sequences with ADLs from the ADL videos (train_without_implicit_adls)
- Only ADLs from the ADL videos and the fall videos, but no fall sequences (train_only_adls)

The first training was done with the training_without_explicit_adls file and one epoch. The model couldn't detect any falls in the video used for testing, but it also didn't detect any false positives.  
The model is saved in keras and h5 format, as the keras format saved on the server can not be loaded on the local windows machine, even though both use the same tensorflow version.

To have more positive training data, the fall sequences are randomly shifted in both x and y directions 20 times.

## Big models training

One big model structure was created with 3 conv layers with 128, 64 and 32 filters, followed by two lstm layers with 100 and 50 units, then the output layer.  
This structure was used to train three different models:
 - With all data (_all_data)
 - With only the ADL data (_only_adls)
 - With the fall data and the implicit ADL data extracted from the fall videos (_without_explicit_adls)
 - With the fall data and the explicit ADL data extracted from the ADL videos (_without_implicit_adls)

All models were trained for 50 epochs.

The model with all data wasn't able to detect any falls in the test video, but also didn't detect any false positives.  
The model with only the ADL data detected nothing at all; as expected there were only close to zero outputs.  
The model with the fall data and the implicit ADL data extracted from the fall videos detected a fall in the test video, but also detected a brief false positive a few seconds after the fall in the tested video. This has to be tested with a video of a person sleeping but may be a good sign.  
The model with the fall data and the explicit ADL data extracted from the ADL videos detected a fall in the test video, and no false positives. This is the best result with the test video so far. This has to be tested with a person sleeping as well.

Ideen:
 Bildbereich ausschneiden
 Model Kompilieren mit https://tvm.apache.org/