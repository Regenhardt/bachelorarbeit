import os

import cv2
import numpy

from PoseEstimation.pose_processor import PoseProcessor, Pose, Landmark
import model

Sequence = list[Pose] # Or numpy array with outer dimension being the sequence and inner dimensions being the pose

# Abstract idea:
# Read excel file
# From each row in the first excel sheet ("Fall"), construct video names and get fall end label.
# For each video, the processor should 
#  - run the pose processor to get a list of fall sequences (one per frame between the fall's end and a second before that)
#  - save the list of sequences into a file, named like the video

# Metadata file has the "end" of fall annotation in column K, 
# which is used for labels (fall label starts a second before the end annotation)
# Fall 1 is in row 3, Fall 2 is in row 4, etc.
# Column A has the Scenario number
# Each fall has 5 cams, Cam1 through Cam5, ergo 5 videos.
# Each video is named like "Fall{numScenario}_Cam{numCam}.avi"

class VideoProcessor:
    def __init__(self, frames=45, skip_frames=1, capture_window=1) -> None:
        '''
        frames: number of frames each resulting fall sequence should have
        skip_frames: number of frames to skip between each frame in the sequence
        capture_window: number of seconds to capture before the fall ends
        '''
        self.frame_buffer_size = frames
        self.skip_frames = skip_frames
        self.capture_window = capture_window

    def process_fall(self, video_path: str, end_annotation: int, skip_existing: bool = False) -> str:
        if skip_existing:
            data_file_path = self.get_data_file_path(video_path)
            if os.path.exists(data_file_path):
                return data_file_path
        elif os.path.exists(self.get_data_file_path(video_path)):
            os.remove(self.get_data_file_path(video_path))
        
        cap = cv2.VideoCapture(video_path)
        if not cap.isOpened():
            raise FileNotFoundError(f"Could not open video file {video_path}")
        cap.set(cv2.CAP_PROP_POS_MSEC, 0)
        self.capture_fall_sequences(cap, end_annotation)
        cap.release()

        return self.save_sequences(video_path)

    def capture_fall_sequences(self, cap: cv2.VideoCapture, end_annotation: int) -> None:
        '''
        Capture fall sequences from a video. Each frame between the end of the fall and a second before that is captured as the last frame in a sequence.
        Saves the resulting sequences in self.sequences.
        '''
        self.sequences = []
        processor = PoseProcessor(frames=self.frame_buffer_size, skip_frames=self.skip_frames)
        while True:
            ret, frame = cap.read()
            if not ret:
                break

            if self.is_before_fall_label(cap, end_annotation):
                # Process and build buffer
                processor.process(frame)
            elif self.is_before_end_of_fall_label(cap, end_annotation):
                # Process and save resulting fall sequences
                seq = processor.process_and_get_current_sequence(frame)
                if seq is not None:
                    self.sequences.append(seq)
            else:
                break

    def process_adls(self, video_path: str, skip_existing: bool = False) -> str:
        if skip_existing:
            data_file_path = self.get_data_file_path(video_path)
            if os.path.exists(data_file_path):
                return data_file_path
        elif os.path.exists(self.get_data_file_path(video_path)):
            os.remove(self.get_data_file_path(video_path))
        
        cap = cv2.VideoCapture(video_path)
        if not cap.isOpened():
            raise FileNotFoundError(f"Could not open video file {video_path}")
        cap.set(cv2.CAP_PROP_POS_MSEC, 0)
        self.capture_adl_sequences(cap)
        cap.release()

        return self.save_sequences(video_path)
    
    def capture_adl_sequences(self, cap: cv2.VideoCapture) -> None:
        '''
        Capture ADL sequences from a video. There are many ADLs in a video, so sequences are captured without overlap.
        Saves the resulting sequences in self.sequences.
        '''
        self.sequences = []
        processor = PoseProcessor(frames=self.frame_buffer_size, skip_frames=self.skip_frames)

        # Instead of one sequence per frame, this will capture one sequence per loop
        while True:
            ret = False;
            # Capture until just before we have a full sequence (buffer size -1)
            for _ in range(self.frame_buffer_size - 1):
                ret, frame = cap.read()
                if not ret:
                    break
                processor.process(frame)

            if not ret:
                break

            #  Then capture until processor has_full_sequence and save sequence
            while True:
                ret, frame = cap.read()
                if not ret:
                    break
                seq = processor.process_and_get_current_sequence(frame)
                if seq is not None and processor.has_full_sequence():
                    self.sequences.append(seq)
                    break

            if not ret:
                break

    def process_adls_from_fall_video(self, video_path: str, start_annotation: int, end_annotation: int, skip_existing: bool = False) -> str:
        if skip_existing:
            data_file_path = self.get_data_file_path(video_path, "_adls")
            if os.path.exists(data_file_path):
                return data_file_path
        
        cap = cv2.VideoCapture(video_path)
        cap.set(cv2.CAP_PROP_POS_MSEC, 0)
        self.capture_adl_sequences_from_fall_video(cap, start_annotation, end_annotation)
        cap.release()

        return self.save_sequences(video_path, "_adls")
    
    def capture_adl_sequences_from_fall_video(self, cap: cv2.VideoCapture, fall_start_annotation: int, fall_end_annotation: int) -> None:
        '''
        Capture ADL sequences from a fall video. There are many ADLs in a video, so sequences are captured without overlap.
        As the fall should not be included, the sequences are captured from the beginning of the video until the start of the fall.
        Saves the resulting sequences in self.sequences.
        '''
        self.sequences = []
        fall_start_msec = fall_start_annotation * 1000
        fall_end_msec = fall_end_annotation * 1000
        processor = PoseProcessor(frames=self.frame_buffer_size, skip_frames=self.skip_frames)

        # Instead of one sequence per frame, this will capture one sequence per loop
        # First capture ADL sequences up until the fall starts
        while cap.get(cv2.CAP_PROP_POS_MSEC) < fall_start_msec:
            ret = False;
            # Capture until just before we have at least one full sequence (buffer size -1)
            for _ in range(self.frame_buffer_size - 1):
                ret, frame = cap.read()
                if not ret:
                    break
                if cap.get(cv2.CAP_PROP_POS_MSEC) > fall_start_msec:
                    break
                processor.process(frame)

            if not ret or cap.get(cv2.CAP_PROP_POS_MSEC) > fall_start_msec:
                break

            # Then capture until processor has_full_sequence and save sequence
            while True:
                ret, frame = cap.read()
                if not ret:
                    break
                if cap.get(cv2.CAP_PROP_POS_MSEC) > fall_start_msec:
                    break
                seq = processor.process_and_get_current_sequence(frame)
                if seq is not None and processor.has_full_sequence():
                    self.sequences.append(seq)
                    break

            if not ret:
                break

        # Then start capturing ADL sequences after the end of the fall
        cap.set(cv2.CAP_PROP_POS_MSEC, fall_end_msec)
        while True:
            ret = False;
            # Capture until just before we have at least one full sequence (buffer size -1)
            for _ in range(self.frame_buffer_size - 1):
                ret, frame = cap.read()
                if not ret:
                    break
                processor.process(frame)

            if not ret:
                break

            # Then capture until processor has_full_sequence and save sequence
            while True:
                ret, frame = cap.read()
                if not ret:
                    break
                seq = processor.process_and_get_current_sequence(frame)
                if seq is not None and processor.has_full_sequence():
                    self.sequences.append(seq)
                    break

            if not ret:
                break

    def get_data_file_path(self, video_path: str, suffix: str = "") -> str:
        video_file_name = os.path.basename(video_path)
        file_name = os.path.splitext(video_file_name)[0]
        data_file_name = f"{file_name}{suffix}.npy"
        directory = os.path.dirname(video_path)

        return os.path.join(directory, data_file_name)

    def save_sequences(self, video_path: str, suffix: str = "") -> str:
        '''
        Write every sequence to a file named after the video. Each line in the file is a sequence.
        Each sequence has a shape of (frame_buffer_size, 33, 3) so just writing it to a string would be truncated.

        Sequences are saved as a numpy array of shape (num_sequences, frame_buffer_size, 33, 3)

        Returns the path to the file containing the sequences.
        '''
        data_file_path = self.get_data_file_path(video_path, suffix)
        arr = numpy.empty((0, self.frame_buffer_size, model.num_landmarks, model.num_coordinates)) if len(self.sequences) == 0 else numpy.array(self.sequences)
        numpy.save(data_file_path, arr)

        return data_file_path

    def load_sequences(self, video_path: str, suffix: str = "") -> numpy.ndarray:
        '''
        Load fall sequences from a file named after the video.
        Sequences are returned as a numpy array of shape (num_sequences, frame_buffer_size, 33, 3)
        '''
        data_file_path = self.get_data_file_path(video_path, suffix)

        loaded_data = numpy.load(data_file_path, allow_pickle=True)
        if loaded_data.shape[0] == 0:
            loaded_data = loaded_data.reshape((0, model.num_frames, model.num_landmarks, model.num_coordinates))

        return loaded_data
    
    def is_before_fall_label(self, cap, end_annotation):
        label_end_msec = end_annotation * 1000
        label_start_msec = label_end_msec - self.capture_window * 1000
        return cap.get(cv2.CAP_PROP_POS_MSEC) < label_start_msec
    
    def is_before_end_of_fall_label(self, cap, end_annotation):
        return cap.get(cv2.CAP_PROP_POS_MSEC) < end_annotation * 1000