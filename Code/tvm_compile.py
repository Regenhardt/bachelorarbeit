import os
import tensorflow as tf
import tf2onnx
from tvm.driver import tvmc

model_dir = '../model'
model_name = '3c_128f_64f_32f_2l_100u_50u_without_implicit_adls.h5'

onnx_model_name = model_name.split('.')[0] + '.onnx'
absolute_onnx_path = os.path.join(os.path.abspath(model_dir), onnx_model_name)
if not os.path.exists(absolute_onnx_path):
    # Load your Keras model
    keras_model = tf.keras.models.load_model(os.path.join(model_dir, model_name))

    # Convert the Keras model to ONNX
    onnx_model, _ = tf2onnx.convert.from_keras(keras_model)

    # Save the ONNX model
    with open(absolute_onnx_path, "wb") as f:
        f.write(onnx_model.SerializeToString())

tvmc_model = tvmc.load(absolute_onnx_path)
#tvmc_model.summary()
tvmc.tune(tvmc_model, target="llvm")
#package = tvmc.compile(tvmc_model, target="llvm")