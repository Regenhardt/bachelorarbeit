import datetime
import os
import model
import numpy as np
from video_processor import VideoProcessor
from utils import timestamp
import KUL_data
from typing import NamedTuple

class TrainingData(NamedTuple):
    '''
    A class to hold the training data for the model. It contains the fall data, the explicit ADL data, and the implicit ADL data.
    Each of these is a 3D numpy array of shape (num_sequences, num_frames, num_landmarks, num_coordinates).
    '''
    fall_data: np.ndarray
    adl_data: np.ndarray
    fall_adl_data: np.ndarray

    def get_fall_data(self) -> np.ndarray:
        '''Returns the fall data from fall videos.'''
        return self.fall_data
    
    def get_explicit_adl_data(self) -> np.ndarray:
        '''Returns the ADL data from ADL videos.'''
        return self.adl_data
    
    def get_implcit_adl_data(self) -> np.ndarray:
        '''Returns the ADL data from fall videos.'''
        return self.fall_adl_data
    
    def get_all_adl_data(self) -> np.ndarray:
        '''Returns the ADL data from both ADL and fall videos.'''
        return np.concatenate((self.adl_data, self.fall_adl_data))

default_model_dir = "../model"

video_processor = VideoProcessor(model.num_frames, model.capture_window)


def preprocess_fall_video(
    fall_file: str, start_annotation: datetime.time, end_annotation: datetime.time, skip_existing=True
) -> str:
    """
    Processes a fall video, returning the path to the file containing the sequences.
    We're assuming the process doesn't get killed in the middle of processing a fall video, so we can load a fall video's sequences from a file if it exists.
    """
    end_annotation_seconds: int = (
        end_annotation.hour * 3600 + end_annotation.minute * 60 + end_annotation.second
    )
    start_annotation_seconds: int = (
        start_annotation.hour * 3600
        + start_annotation.minute * 60
        + start_annotation.second
    )

    fall_data_file = video_processor.process_fall(
        fall_file, end_annotation_seconds, skip_existing=skip_existing
    )

    fall_adl_data_file = video_processor.process_adls_from_fall_video(
        fall_file, start_annotation_seconds, end_annotation_seconds, skip_existing=skip_existing
    )

    return fall_data_file, fall_adl_data_file


def random_shift(sequence: np.ndarray) -> np.ndarray:
    # The sequence might have 2 or 3 coordinates - just lose the last one if it's there
    sequence = sequence[:, :, :2]
    # Calculate the min and max values for each dimension
    min_values = sequence.min(axis=(0, 1))
    max_values = sequence.max(axis=(0, 1))

    # Calculate the maximum shift in each direction
    max_shifts = np.minimum(min_values, 1 - max_values)

    # Generate a random shift within the allowable range
    shifts = (np.random.rand(2) - 0.5) * 2 * max_shifts

    # Apply the shift to the entire sequence
    shifted_sequence = sequence + shifts

    return shifted_sequence


def preprocess_scenario(
    scenario: KUL_data.ScenarioData,
    random_shift_fall_data: int = 0,
    skip_existing = True,
) -> tuple[np.ndarray, np.ndarray]:
    """
    Processes a scenario with MediaPipe, returning the fall and ADL data. Data is saved to files named after each source video:
    Fall1_Cam1.avi -> Fall1_Cam1.npy + Fall1_Cam1_adls.npy

    Args:
        scenario: The scenario to process
        random_shift_fall_data: Whether to randomly shift the fall data after reading it from the file. This is useful for data augmentation and will not manipulate the data in the file.

    Returns:
        A tuple containing the fall and ADL data, each an array of sequences.
    """
    # fall_data contains many sequences. Each sequence is a 3D array of shape (num_frames, num_landmarks, num_coordinates).
    fall_data = None
    fall_adl_data = None
    for video_path in scenario.get_video_file_paths():
        try:
            fall_data_file, fall_adl_data_file = preprocess_fall_video(
                video_path, scenario.Start, scenario.End, skip_existing=skip_existing
            )
            new_fall_data = video_processor.load_sequences(fall_data_file)
            fall_data = np.concatenate(
                (fall_data, new_fall_data)
            ) if fall_data is not None else new_fall_data
            new_fall_adl_data = video_processor.load_sequences(fall_adl_data_file)
            fall_adl_data = np.concatenate(
                (fall_adl_data, new_fall_adl_data)
            ) if fall_adl_data is not None else new_fall_adl_data
        except Exception as e:
            print("Error processing video", video_path)
            print(e)

    model.validate_inputs(fall_data, fall_adl_data, log=False)

    # fall_data has many sequences. Shift each sequence by a random amount.
    if len(fall_data) > 0 and fall_data.shape[1] > 0:
        shifted_data = None
        for _ in range(random_shift_fall_data):
            new_shifted_data = np.array([random_shift(seq) for seq in fall_data])
            shifted_data = np.concatenate(
                (shifted_data, new_shifted_data)
            ) if shifted_data is not None else new_shifted_data
        fall_data = np.concatenate((fall_data, shifted_data))

    return fall_data, fall_adl_data


def preprocess_adl_video(adl_file: str, skip_existing = True) -> str:
    """
    Processes an ADL video, returning the sequences. Data is saved to a file named after the source video:
    ADL1_Cam1.avi -> ADL1_Cam1.npy
    We're assuming the process doesn't get killed in the middle of processing an ADL video, so we can load an ADL video's sequences from a file if it exists.
    """
    adl_data_file = video_processor.process_adls(adl_file, skip_existing=skip_existing)

    return video_processor.load_sequences(adl_data_file)

def get_all_data(skip_existing = True) -> TrainingData:
    '''Returns all the data extracted from the KUL dataset. Extracts it first if the data is not already extracted.'''
    fall_metadata: list[KUL_data.ScenarioData] = KUL_data.get_metadata()
    adl_metadata: list[str] = KUL_data.get_adl_metadata()

    fall_data: np.ndarray | None = None
    adl_data: np.ndarray | None = None
    fall_adl_data: np.ndarray | None = None

    for scenario in fall_metadata:
        try:
            sc_fall_data, sc_fall_adl_data = preprocess_scenario(scenario, random_shift_fall_data=20, skip_existing=skip_existing)
            fall_data = np.concatenate((fall_data, sc_fall_data)) if fall_data is not None else sc_fall_data
            fall_adl_data = np.concatenate((fall_adl_data, sc_fall_adl_data)) if fall_adl_data is not None else sc_fall_adl_data
        except Exception as e:
            print("Error processing scenario", scenario.Scenario)
            print(e)

    for adl in adl_metadata:
        try:
            explicit_adl_data = preprocess_adl_video(adl, skip_existing=skip_existing)
            adl_data = np.concatenate((adl_data, explicit_adl_data)) if adl_data is not None else explicit_adl_data
        except Exception as e:
            print("Error processing ADL file", adl)
            print(e)

    return TrainingData(fall_data, adl_data, fall_adl_data)

if __name__ == "__main__":
    import sys

    if "--help" in sys.argv or "-h" in sys.argv:
        print(
            """
Usage: python preprocessor.py [options]

Options:
    -h, --help: Show this help message.
    --local: Use local data instead of environment variables
    --force: Also preprocess videos that already have associated data files, instead of skipping them..
"""
        )
        sys.exit(0)

    # First, make sure VID_DIR environment variable is set, unless "local" was passed as an argument
    if not "--local" in sys.argv:
        if os.environ.get("VID_DIR") is None:
            raise Exception("VID_DIR environment variable is not set")
        
    skip_existing = not "--force" in sys.argv

    # Get metadata and process all scenarios and ADLs
    fall_metadata: list[KUL_data.ScenarioData] = KUL_data.get_metadata()
    adl_metadata: list[str] = KUL_data.get_adl_metadata()

    for scenario in fall_metadata:
        try:
            if not skip_existing:
                print('Preprocessing scenario {} regardless of existing data'.format(scenario.Scenario))
            preprocess_scenario(scenario, skip_existing=skip_existing)
        except Exception as e:
            print("Error processing scenario", scenario.Scenario)
            print(e)
    for adl in adl_metadata:
        try:
            if not skip_existing:
                print('Preprocessing adl {} regardless of existing data'.format(adl))
            preprocess_adl_video(adl, skip_existing=skip_existing)
        except Exception as e:
            print("Error processing ADL file", adl)
            print(e)

    print(timestamp(), "Done!")
