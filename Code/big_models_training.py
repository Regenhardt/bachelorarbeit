from datetime import datetime
import os
import sys
import numpy as np
import model
import preprocessor
import pickle
from preprocessor import TrainingData

# Make sure environment is set up
if "--help" in sys.argv or "-h" in sys.argv:
    print(
        """
Usage: python train_big_models.py [options]

Options:
    --help, -h: Show this message
    --weighted, -w: Weight the fall data more heavily than the ADL data
    --force, -f: Force retraining of models
"""
    )
    sys.exit(0)

if os.environ.get("SAVE_DIR") is None:
    raise Exception("SAVE_DIR environment variable is not set")
if os.environ.get("VID_DIR") is None:
    raise Exception("VID_DIR environment variable is not set")

force = "--force" in sys.argv or "-f" in sys.argv
save_dir = os.environ.get("SAVE_DIR")
save_format = "h5"

filters = [128, 64, 32]
units = [100, 50]
epochs = 50
class_weight = {0: 1, 1: 50} if "--weighted" in sys.argv or "-w" in sys.argv else None

# Gather data
data: TrainingData = preprocessor.get_all_data()
fall_data = data.get_fall_data()
adl_data = data.get_explicit_adl_data()
fall_adl_data = data.get_implcit_adl_data()
all_adl_data = data.get_all_adl_data()

# Train models

def get_model_file_name(description: str, structure_name: str, format: str) -> str:
    return os.path.join(save_dir, f"{structure_name}_{description}.{format}")

histories = {}

## First with all data
model_description = "all_data"
model_name = model.get_model_structure_name(filters, units, class_weight)
model_file = get_model_file_name(model_description, model_name, save_format)
if os.path.exists(model_file) and not force:
    print("[big_models_training] Model {} already exists. Skipping".format(model_name))
else:
    if os.path.exists(model_file):
        print("[big_models_training] Model {} already exists. Deleting old model and retraining.".format(model_name))
        os.remove(model_file)
    startTime = datetime.now()
    def set_history(h):
        histories[model_name] = h
    trained_model = model.train_model(
        fall_data,
        all_adl_data,
        filters_list=filters,
        units_list=units,
        epochs=epochs,
        class_weight=class_weight,
        history_callback=set_history,
    )
    trained_model.save(model_file)
    with open(model_file.replace(".h5", "_history"), "wb") as f:
        pickle.dump(histories[model_name], f)
    print(
        "[big_models_training] Training time {}: ".format(model_name),
        datetime.now() - startTime,
    )

## Then with only explicit ADLs
model_description = "without_implicit_adls"
model_name = model.get_model_structure_name(filters, units, class_weight)
model_file = get_model_file_name(model_description, model_name, save_format)
if os.path.exists(model_file) and not force:
    print("[big_models_training] Model {} already exists. Skipping".format(model_name))
else:
    if os.path.exists(model_file):
        print("[big_models_training] Model {} already exists. Deleting old model and retraining.".format(model_name))
        os.remove(model_file)
    startTime = datetime.now()
    def set_history(h):
        histories[model_name] = h
    trained_model = model.train_model(
        fall_data,
        adl_data,
        filters_list=filters,
        units_list=units,
        epochs=epochs,
        class_weight=class_weight,
        history_callback=set_history,
    )
    trained_model.save(model_file)
    with open(model_file.replace(".h5", "_history"), "wb") as f:
        pickle.dump(histories[model_name], f)
    print(
        "[big_models_training] Training time {}: ".format(model_name),
        datetime.now() - startTime,
    )

## Then with only implicit ADLs
model_description = "without_explicit_adls"
model_name = model.get_model_structure_name(filters, units, class_weight)
model_file = get_model_file_name(model_description, model_name, save_format)
if os.path.exists(model_file) and not force:
    print("[big_models_training] Model {} already exists. Skipping".format(model_name))
else:
    if os.path.exists(model_file):
        print("[big_models_training] Model {} already exists. Deleting old model and retraining.".format(model_name))
        os.remove(model_file)
    startTime = datetime.now()
    def set_history(h):
        histories[model_name] = h
    trained_model = model.train_model(
        fall_data,
        fall_adl_data,
        filters_list=filters,
        units_list=units,
        epochs=epochs,
        class_weight=class_weight,
        history_callback=set_history,
    )
    trained_model.save(model_file)
    with open(model_file.replace(".h5", "_history"), "wb") as f:
        pickle.dump(histories[model_name], f)
    print(
        "[big_models_training] Training time {}: ".format(model_name),
        datetime.now() - startTime,
    )

## Then with explicit and implicit ADLs but without fall data
model_description = "only_adls"
model_name = model.get_model_structure_name(filters, units, class_weight)
model_file = get_model_file_name(model_description, model_name, save_format)
if os.path.exists(model_file) and not force:
    print("[big_models_training] Model {} already exists. Skipping".format(model_name))
else:
    if os.path.exists(model_file):
        print("[big_models_training] Model {} already exists. Deleting old model and retraining.".format(model_name))
        os.remove(model_file)
    startTime = datetime.now()
    history = None;
    def set_history(h):
        histories[model_name] = h
    trained_model = model.train_model(
        np.empty((0, *fall_data.shape[1:])),
        np.concatenate((adl_data, fall_adl_data)),
        filters_list=filters,
        units_list=units,
        epochs=epochs,
        class_weight=class_weight,
        history_callback=set_history,
    )
    trained_model.save(model_file)
    with open(model_file.replace(".h5", "_history"), "wb") as f:
        pickle.dump(histories[model_name], f)
    print(
        "[big_models_training] Training time {}: ".format(model_name),
        datetime.now() - startTime,
    )

print("[big_models_training] Done")
