import os
import pandas as pd
import datetime
from typing import NamedTuple

scenarios_not_to_use = ["1"]
latest_validated_scenario = 20

kul_metadata_file = "../Data/KUL_Metadata.xlsx"
video_directory = "../Data/KUL"

if os.environ.get("VID_DIR") is not None:
    video_directory = os.environ.get("VID_DIR")

# Make sure there are videos in the video directory
if not os.path.isdir(video_directory):
    raise Exception("Video directory does not exist: {}".format(video_directory))
if len(os.listdir(video_directory)) == 0:
    raise Exception("Video directory is empty: {}".format(video_directory))

class ScenarioData(NamedTuple):
    Scenario: str
    Start: datetime.time
    End: datetime.time

    def get_video_file_names(self) -> list[str]:
        """Without the path, just the file names"""
        return [f"Fall{self.Scenario}_Cam{i}.avi" for i in range(1, 6)]

    def get_video_file_paths(self) -> list[str]:
        """With the path"""
        return [
            os.path.join(video_directory, video_name)
            for video_name in self.get_video_file_names()
        ]


def get_metadata() -> list[ScenarioData]:
    """Gets the metadata for all fall scenarios from the KUL dataset."""
    # Metadata file has the "end" of fall annotation in column K,
    # which is used for labels (fall label starts a second before the end annotation).
    # The "start" annotation is in column I, which may be used to guard when readong ADLs from a fall video.
    # Fall 1 is in row 3, Fall 2 is in row 4, etc.
    df = pd.read_excel(
        kul_metadata_file,
        skiprows=1,
        usecols=[0, 8, 10],
        names=["Scenario", "Start", "End"],
    )

    metadata: list[ScenarioData] = []
    for index, row in df.iterrows():
        scenario = str(row["Scenario"])
        if scenario in scenarios_not_to_use:
            continue
        start = row["Start"]
        end = row["End"]
        if not isinstance(start, datetime.time):
            start = datetime.time.fromisoformat(start)
        if not isinstance(end, datetime.time):
            end = datetime.time.fromisoformat(end)

        metadata.append(ScenarioData(scenario, start, end))

    return metadata


def get_fall_video_paths() -> list[str]:
    """
    Returns a list of paths to all fall videos.
    """
    metadata: list[ScenarioData] = get_metadata()
    return [path for scenario in metadata for path in scenario.get_video_file_paths()]

def get_adl_metadata() -> list[str]:
    '''
    Returns a list of ADL metadata.
    No need to read the excel file, just return all the ADL videos in the video directory.
    '''

    return [os.path.join(video_directory, file) for file in os.listdir(video_directory) if file.startswith('ADL')]