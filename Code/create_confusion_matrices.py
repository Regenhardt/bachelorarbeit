import os
import sys
import numpy as np
from tensorflow.keras.models import load_model
import tensorflow as tf
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
from preprocessor import TrainingData, get_all_data
import utils

# Make sure environment is set up
if "--help" in sys.argv or "-h" in sys.argv:
    print(
        """
Usage: python create_confusion_matrices.py [options]

Options:
    --help, -h: Show this message
    --force, -f: Also recreate confusion matrices that already exist
"""
    )
    sys.exit(0)

if os.environ.get("SAVE_DIR") is None:
    raise Exception("SAVE_DIR environment variable is not set")
if os.environ.get("VID_DIR") is None:
    raise Exception("VID_DIR environment variable is not set")

save_dir = os.environ.get("SAVE_DIR")
force = "--force" in sys.argv or "-f" in sys.argv

def log(*args):
    print("{}[create_confusion_matrices]".format(utils.timestamp()), *args)

# Load test data
data: TrainingData = get_all_data()
X_test = np.concatenate((data.get_all_adl_data(), data.get_fall_data()))  # Concatenate ADL and fall data

# Create labels
y_test = np.concatenate((np.zeros(data.get_all_adl_data().shape[0]), np.ones(data.get_fall_data().shape[0])))

# Get list of model files
model_files = [f for f in os.listdir(save_dir) if f.endswith('.h5')]

fall_threshold = 0.9

for model_file in model_files:
    model_name = model_file.replace('.h5', '')
    matrix_name = f'{model_name}_confusion_matrix' + str(fall_threshold).replace('.', '_')
    matrix_file = os.path.join(save_dir, f'{matrix_name}.png')
    matrix_data_file = os.path.join(save_dir, f'{matrix_name}.npy')
    if not force and os.path.exists(matrix_file):
        log(f'Confusion matrix for {model_file} already exists, skipping...')
        continue

    if force:
        if os.path.exists(matrix_file):
            os.remove(matrix_file)
            log(f'Removing existing confusion matrix for {model_file}')
        if os.path.exists(matrix_data_file):
            os.remove(matrix_data_file)

    if not os.path.exists(matrix_data_file):
        log(f'Creating confusion matrix for {model_file}')
        # Load model
        model = load_model(os.path.join(save_dir, model_file))
        
        # Predict classes
        y_pred_prob = model.predict(X_test)
        y_pred = (y_pred_prob > fall_threshold).astype(int)
        
        # Compute confusion matrix
        cm = confusion_matrix(y_test, y_pred)
        
        # Save confusion matrix data
        np.save(matrix_data_file, cm)
    else:
        log(f'Loading confusion matrix data for {model_file}')
        cm = np.load(matrix_data_file)
    
    # Plot confusion matrix
    plt.figure(figsize=(10,7))
    sns.heatmap(cm, annot=True, annot_kws={"size": 30})
    plt.xlabel('Predicted', fontsize=20)
    plt.ylabel('Truth', fontsize=20)
    plt.title(f'Confusion Matrix for {model_file}', fontsize=16)
    
    # Save confusion matrix
    plt.savefig(os.path.join(save_dir, matrix_file))
    plt.close()
    log(f'Confusion matrix for {model_name} created and saved')

log('Done!')