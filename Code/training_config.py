default_model_dir: str = "../model"
epochs_list = [50, 100]
units_list_list = [[10], [10, 5], [10, 10], [50, 50]]
filter_list_list = [[], [8], [16], [32], [16, 8]]
model_format = "h5"