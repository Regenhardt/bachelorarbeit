import os
import pickle
import sys
from typing import List, NamedTuple
import numpy as np
from tensorflow.keras.models import load_model
import tensorflow as tf
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
from KUL_data import ScenarioData, get_adl_metadata, get_metadata
from preprocessor import TrainingData, get_all_data, preprocess_adl_video, preprocess_scenario
import utils
from utils import ModelConfig
import model
import matplotlib.pyplot as plt

if os.environ.get("SAVE_DIR") is None:
    raise Exception("SAVE_DIR environment variable is not set")
if os.environ.get("VID_DIR") is None:
    raise Exception("VID_DIR environment variable is not set")

save_dir = os.environ.get("SAVE_DIR")
force = "--force" in sys.argv or "-f" in sys.argv

def log(*args):
    print("{}[create_loss_images]".format(utils.timestamp()), *args)

# Load test data
data: TrainingData = get_all_data()
X_test = np.concatenate((data.get_all_adl_data(), data.get_fall_data()))  # Concatenate ADL and fall data

# Create labels
y_test = np.concatenate((np.zeros(data.get_all_adl_data().shape[0]), np.ones(data.get_fall_data().shape[0])))

model_configs = [
    ModelConfig([8], [10]),
    ModelConfig([16, 8], [10]),
    ModelConfig([8], [10, 5]),
    ModelConfig([16, 8], [10, 5]),
    ModelConfig([128, 64, 32], [10, 5]),
    ModelConfig([16, 8], [100, 50]),
    ModelConfig([128, 64, 32], [100, 50])
]

"""
train_50e100e_models(
    fall_data: np.ndarray | None = None,
    adl_data: np.ndarray | None = None,
    units_list: list[int] = [1],
    batch_size: int = 8,
    validation_split: float = 0.1,
    verbose: int = 1,
    filters_list: list[int] = [],
    class_weight: dict[int, int] | None = None,
) -> tuple[tf.keras.models.Sequential, tf.keras.models.Sequential, dict[str, list[float]]]
"""

fall_videos_metadata: list[ScenarioData] = get_metadata()
adl_videos_metadata: list[str] = get_adl_metadata()

for model_config in model_configs:
    model_name = model.get_model_structure_name(model_config.filters, model_config.units)
    model_file = os.path.join(save_dir, model_name + ".h5")
    model50_file = os.path.join(save_dir, model_name + "_50e.h5")
    model100_file = os.path.join(save_dir, model_name + "_100e.h5")
    history_file = os.path.join(save_dir, model_name + "_history")
    loss_plot_file = os.path.join(save_dir, model_name + "_loss.png")
    cm50_file = os.path.join(save_dir, model_name + "_50e_cm.npy")
    cm100_file = os.path.join(save_dir, model_name + "_100e_cm.npy")

    model50e, model100e, history = None, None, None

    # If models, loss plots and confusion matrices already exist, skip training and load instead
    if not force and utils.file_exists(model50_file) and utils.file_exists(model100_file) and utils.file_exists(history_file):
        log("Model already exists, skipping training", model_name)
        model50e = load_model(model50_file)
        model100e = load_model(model100_file)
        try:
            with open(history_file, "rb") as f:
                history = pickle.load(f)
        except Exception as e:
            log("Failed to load history data for model", model_name, e)
    else:
        # Delete any existing model or history files from partial runs
        for file in [model_file, model50_file, model100_file, history_file, loss_plot_file, cm50_file, cm100_file]:
            if utils.file_exists(file):
                os.remove(file)
        
        # Gather data for all scenarios
        fall_data: np.ndarray = None
        adl_data: np.ndarray = None
        for scenario in fall_videos_metadata:
            sc_fall_data, sc_fall_adl_data = preprocess_scenario(
                scenario, random_shift_fall_data=20
            )
            fall_data = (
                sc_fall_data
                if fall_data is None
                else np.concatenate((fall_data, sc_fall_data))
            )
            adl_data = (
                sc_fall_adl_data
                if adl_data is None
                else np.concatenate((adl_data, sc_fall_adl_data))
            )

        # Add explicit ADL data
        for video_path in adl_videos_metadata:
            explicit_adl_data = preprocess_adl_video(video_path)
            if (
                explicit_adl_data is not None
                and len(explicit_adl_data.shape) > 0
                and explicit_adl_data.shape[0] > 0
            ):
                adl_data = (
                    explicit_adl_data
                    if adl_data is None
                    else np.concatenate((adl_data, explicit_adl_data))
                )

        # Train models
        model50e, model100e, history = model.train_50e100e_models(
            fall_data=fall_data,
            adl_data=adl_data,
            units_list=model_config.units,
            filters_list=model_config.filters,
            verbose=1,
            batch_size=8,
            validation_split=0.1,
        )

        # Save models and history data
        model50e.save(model50_file)
        model100e.save(model100_file)
        try:
            with open(history_file, "wb") as f:
                pickle.dump(history, f)
        except Exception as e:
            log("Failed to save history data for model", model_name, e)

    # Skip loss plot if it already exists
    if not force and os.path.exists(loss_plot_file):
        log("Loss plot already exists, skipping", model_name)
    else:
        # Plot loss
        try:
            plt.figure()
            plt.plot(history["loss"])
            plt.plot(history["val_loss"])
            plt.title("Model loss")
            plt.ylabel("Loss")
            plt.xlabel("Epoch")
            plt.legend(["Train", "Validation"], loc="upper left")
            plt.savefig(loss_plot_file)
            plt.close()
            log("Saved loss plot for model", model_name)
        except Exception as e:
            log("Failed to save loss plot for model", model_name, e)

    # Skip confusion matrices if they already exist
    if not force and os.path.exists(cm50_file) and os.path.exists(cm100_file):
        log("Confusion matrices already exist, skipping", model_name)
    else:
        # Evaluate models
        try:
            fall_threshold = 0.9
            y_pred50e = model50e.predict(X_test)
            y_pred100e = model100e.predict(X_test)
            y_pred50e = (y_pred50e > fall_threshold).astype(int)
            y_pred100e = (y_pred100e > fall_threshold).astype(int)
            cm50e = confusion_matrix(y_test, y_pred50e)
            cm100e = confusion_matrix(y_test, y_pred100e)
            np.save(cm50_file, cm50e)
            np.save(cm100_file, cm100e)
            log("Saved confusion matrices for model", model_name)

            # Plot confusion matrices
            plt.figure(figsize=(10, 7))
            sns.heatmap(cm50e, annot=True, annot_kws={"size": 30})
            plt.xlabel("Predicted", fontsize=20)
            plt.ylabel("Truth", fontsize=20)
            plt.title(f"Confusion Matrix for {model_name} (50 epochs)", fontsize=16)
            plt.savefig(cm50_file.replace(".npy", ".png"))
            plt.close()

            plt.figure(figsize=(10, 7))
            sns.heatmap(cm100e, annot=True, annot_kws={"size": 30})
            plt.xlabel("Predicted", fontsize=20)
            plt.ylabel("Truth", fontsize=20)
            plt.title(f"Confusion Matrix for {model_name} (100 epochs)", fontsize=16)
            plt.savefig(cm100_file.replace(".npy", ".png"))
            plt.close()
            log("Saved confusion matrix plots for model", model_name)
        except Exception as e:
            log("Failed to evaluate model", model_name, e)

log("Done!")