import argparse
from boxsdk import OAuth2, Client
import requests
import shutil
import os
import utils


def authenticate_box(client_id, client_secret, access_token):
    """Authenticate with Box and return the client object."""
    auth = OAuth2(
        client_id=client_id, client_secret=client_secret, access_token=access_token
    )
    return Client(auth)


def download_file_from_url(url: str, file_name: str, download_path="."):
    """Download a file from a URL."""
    local_filename = os.path.join(download_path, file_name)
    with requests.get(url, stream=True) as r:
        with open(local_filename, "wb") as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)
    return local_filename


def download_folder_contents(folder, download_path="."):
    """Download all contents of the folder."""
    folder_items = folder.get_items(limit=2)
    print("Received folder items.")
    num_items = 0
    for item in folder_items:
        num_items += 1
    print("Counted {} folder items.".format(num_items))
    folder_items = folder.get_items(limit=2)
    current_item = 0
    while True:
        try:
            item = folder_items.next()
            if item is None:
                break
            current_item += 1
            print(
                "Downloading item {} of {}: {}".format(
                    current_item, num_items, item.name
                )
            )
            download_file_from_url(item.get_download_url(), item.name, download_path)
        except StopIteration:
            break
    print(utils.timestamp(), "Downloaded {} items.".format(current_item))


def main(client_id, client_secret, access_token, shared_link, download_path):
    client: Client = authenticate_box(client_id, client_secret, access_token)
    print(utils.timestamp(), "Authenticated with Box.")
    folder = client.get_shared_item(shared_link)
    print(utils.timestamp(), f"Folder ID: {folder.id}")
    download_folder_contents(folder, download_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Download contents of a shared Box folder."
    )
    parser.add_argument("client_id", help="Box Client ID")
    parser.add_argument("client_secret", help="Box Client Secret")
    parser.add_argument("access_token", help="Box Access Token")
    parser.add_argument("shared_link", help="Box Shared Folder Link")
    parser.add_argument(
        "--download_path", default=".", help="Path to save downloaded files"
    )

    args = parser.parse_args()

    main(
        args.client_id,
        args.client_secret,
        args.access_token,
        args.shared_link,
        args.download_path,
    )
