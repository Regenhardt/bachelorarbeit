import tensorflow as tf
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from typing import Callable
from tensorflow.keras.callbacks import History


# Number of frames (has to match number of poses per sequence)
num_frames = 45

# Number of landmarks per frame/pose
num_landmarks = 33

# Number of coordinates per landmark point
num_coordinates = 2

# Number of seconds before the end annotation to include in the training data
capture_window = 2

input_shape = (num_frames, num_landmarks, num_coordinates)

def log(*args):
    print("[model]", *args)

def get_model_structure_name(
    filter_list: list[int], 
    units_list: list[int], 
    class_weights: dict[int, int] | None = None
) -> str:
    if filter_list is None or units_list is None:
        raise ValueError(
            "filter_list and units_list must not be None. Pass empty lists instead."
        )

    model_name_filters = "{}c".format(len(filter_list))
    for filters in filter_list:
        model_name_filters += "_{}f".format(filters)
    model_name_layers = "{}l".format(len(units_list))
    for units in units_list:
        model_name_layers += "_{}u".format(units)
    if len(filter_list) > 0:
        model_name = "{}_{}".format(model_name_filters, model_name_layers)
    else:
        model_name = model_name_layers

    if class_weights is not None:
        model_name += "_weighted"

    return model_name


def _add_lstm_layers(
    model: tf.keras.models.Sequential, input_shape: np.ndarray, units_list: list[int]
) -> None:
    for i, units in enumerate(units_list):
        if i == 0 and input_shape is not None:
            model.add(
                tf.keras.layers.LSTM(
                    units,
                    input_shape=input_shape,
                    return_sequences=(i < len(units_list) - 1),
                )
            )
        else:
            model.add(
                tf.keras.layers.LSTM(
                    units,
                    return_sequences=(i < len(units_list) - 1),
                )
            )


def create_conv_lstm_model(
    input_shape: np.ndarray,
    num_output_labels: int,
    filters_list: list[int],
    units_list: list[int],
    class_weight: dict[int, int] | None = None,
) -> tf.keras.models.Sequential:
    # Make sure, filters and units lists contain only integers
    if filters_list is not None and not all(isinstance(x, int) for x in filters_list):
        raise ValueError("filters_list must contain only integers")
    if units_list is not None and not all(isinstance(x, int) for x in units_list):
        raise ValueError("units_list must contain only integers")

    model_name = get_model_structure_name(filters_list, units_list, class_weight)
    print("Creating model: {}".format(model_name))

    if filters_list is None or len(filters_list) < 1:
        return create_lstm_model(input_shape, num_output_labels, units_list)

    print("Creating model with {} filters and {} units".format(filters_list, units_list))
    model = tf.keras.models.Sequential(name=model_name)
    model.add(tf.keras.layers.Reshape((*input_shape, 1), input_shape=input_shape))

    # Add Conv2D layers
    for num_filters in filters_list:
        model.add(
            tf.keras.layers.TimeDistributed(
                tf.keras.layers.Conv2D(
                    num_filters, (3, 3), activation="relu", padding="same"
                )
            )
        )

    model.add(tf.keras.layers.Reshape((input_shape[0], -1)))

    _add_lstm_layers(model, input_shape=None, units_list=units_list)

    # Add the output layer
    model.add(
        tf.keras.layers.Dense(
            num_output_labels,
            activation="sigmoid" if num_output_labels == 1 else "softmax",
        )
    )

    return model


def create_lstm_model(
    input_shape: np.ndarray, num_output_labels: int, units_list: list[int], class_weight: dict[int, int] | None = None
) -> tf.keras.models.Sequential:
    model = tf.keras.models.Sequential(name=get_model_structure_name([], units_list, class_weight))

    model.add(
        tf.keras.layers.Reshape(
            (input_shape[0], input_shape[1] * input_shape[2]), input_shape=input_shape
        )
    )
    _add_lstm_layers(model, input_shape, units_list)

    # Add the output layer
    model.add(
        tf.keras.layers.Dense(
            num_output_labels,
            activation="sigmoid" if num_output_labels == 1 else "softmax",
        )
    )

    return model


def validate_inputs(
    fall_data: np.ndarray | None, adl_data: np.ndarray | None, log=True
) -> None:
    if fall_data is None:
        raise ValueError("Fall data is None")

    if adl_data is None:
        raise ValueError("ADL data is None")

    if log:
        print("Shape of fall data: {}".format(fall_data.shape))
        print("Shape of ADL data: {}".format(adl_data.shape))

    # Validate fall data
    if fall_data.shape[1] != num_frames:
        raise ValueError(
            "Fall data must have {} frames/poses per sequence but has {}".format(
                num_frames, fall_data.shape[1]
            )
        )

    if fall_data.shape[2] != num_landmarks:
        raise ValueError(
            "Fall data must have {} landmarks per pose but has {}".format(
                num_landmarks, fall_data.shape[2]
            )
        )

    if fall_data.shape[3] < num_coordinates:
        raise ValueError(
            "Fall data must have at least {} coordinates per landmark but has {}".format(
                num_coordinates, fall_data.shape[3]
            )
        )

    # Validate ADL data
    if adl_data.shape[1] != num_frames:
        raise ValueError(
            "ADL data must have {} frames/poses but has {}".format(
                num_frames, adl_data.shape[1]
            )
        )

    if adl_data.shape[2] != num_landmarks:
        raise ValueError(
            "ADL data must have {} landmarks but has {}".format(
                num_landmarks, adl_data.shape[2]
            )
        )

    if adl_data.shape[3] < num_coordinates:
        raise ValueError(
            "ADL data must have at least {} coordinates per landmark but has {}".format(
                num_coordinates, adl_data.shape[3]
            )
        )

    if log:
        print("Data is valid!")


def train_model(
    fall_data: np.ndarray | None = None,
    adl_data: np.ndarray | None = None,
    existing_model: tf.keras.models.Sequential | None = None,
    units_list: list[int] = [1],
    epochs: int = 1,
    batch_size: int = 8,
    validation_split: float = 0.1,
    verbose: int = 1,
    filters_list: list[int] = [],
    class_weight: dict[int, int] | None = None,
    history_callback: Callable[[History], None] | None = None,
) -> tf.keras.models.Sequential:
    # Validate inputs
    validate_inputs(fall_data, adl_data)

    # if there are more than 2 coordinates per landmark, reduce the data to 2 coordinates
    if fall_data.shape[3] > num_coordinates:
        print(
            "Reducing fall data from {} coordinates to {}".format(
                fall_data.shape[3], num_coordinates
            )
        )
        fall_data = fall_data[:, :, :, :num_coordinates]

    if adl_data.shape[3] > num_coordinates:
        print(
            "Reducing ADL data from {} coordinates to {}".format(
                adl_data.shape[3], num_coordinates
            )
        )
        adl_data = adl_data[:, :, :, :num_coordinates]

    if existing_model is not None:
        model = existing_model
    else:
        # Create new model
        model = create_conv_lstm_model(
            input_shape, 1, filters_list, units_list, class_weight
        )
        model.compile(
            optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"]
        )

    # Split both fall and ADL sequences into training and validation sets, unless for some reason there is no data
    if fall_data.shape[0] >= 2:
        fall_train, fall_val = train_test_split(fall_data, test_size=validation_split)
    else:
        fall_train = fall_data
        fall_val = fall_data
    adl_train, adl_val = train_test_split(adl_data, test_size=validation_split)

    # Create the training data
    x_train = np.concatenate((fall_train, adl_train))
    # Ones for fall, zeros for ADL
    y_train = np.concatenate(
        (np.ones(fall_train.shape[0]), np.zeros(adl_train.shape[0]))
    )
    x_train, y_train = shuffle(x_train, y_train)

    # Create the validation data
    x_val = np.concatenate((fall_val, adl_val))
    y_val = np.concatenate((np.ones(fall_val.shape[0]), np.zeros(adl_val.shape[0])))
    x_val, y_val = shuffle(x_val, y_val)

    # Train the model
    if class_weight is None:
        class_weight = {0: 1, 1: 1}
    history = model.fit(
        x_train,
        y_train,
        epochs=epochs,
        batch_size=batch_size,
        validation_data=(x_val, y_val),
        verbose=verbose,
        class_weight=class_weight,
    )

    if history_callback is not None:
        history_callback(history)

    return model

def train_50e100e_models(
    fall_data: np.ndarray | None = None,
    adl_data: np.ndarray | None = None,
    units_list: list[int] = [1],
    batch_size: int = 8,
    validation_split: float = 0.1,
    verbose: int = 1,
    filters_list: list[int] = [],
    class_weight: dict[int, int] | None = None,
) -> tuple[tf.keras.models.Sequential, tf.keras.models.Sequential, dict[str, list[float]]]:
    """
    Trains a model for 50 epochs, then copies the model and trains it for 50 more.

    Returns the model after 50 epochs, the model after 100 epochs, and the history of both models.
    """
    # Validate inputs
    validate_inputs(fall_data, adl_data)

    # if there are more than 2 coordinates per landmark, reduce the data to 2 coordinates
    if fall_data.shape[3] > num_coordinates:
        print(
            "Reducing fall data from {} coordinates to {}".format(
                fall_data.shape[3], num_coordinates
            )
        )
        fall_data = fall_data[:, :, :, :num_coordinates]

    if adl_data.shape[3] > num_coordinates:
        print(
            "Reducing ADL data from {} coordinates to {}".format(
                adl_data.shape[3], num_coordinates
            )
        )
        adl_data = adl_data[:, :, :, :num_coordinates]

    # Create new model
    model = create_conv_lstm_model(
        input_shape, 1, filters_list, units_list, class_weight
    )
    model.compile(
        optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"]
    )

    # Split both fall and ADL sequences into training and validation sets, unless for some reason there is no data
    if fall_data.shape[0] >= 2:
        fall_train, fall_val = train_test_split(fall_data, test_size=validation_split)
    else:
        fall_train = fall_data
        fall_val = fall_data
    adl_train, adl_val = train_test_split(adl_data, test_size=validation_split)

    # Create the training data
    x_train = np.concatenate((fall_train, adl_train))
    # Ones for fall, zeros for ADL
    y_train = np.concatenate(
        (np.ones(fall_train.shape[0]), np.zeros(adl_train.shape[0]))
    )
    x_train, y_train = shuffle(x_train, y_train)

    # Create the validation data
    x_val = np.concatenate((fall_val, adl_val))
    y_val = np.concatenate((np.ones(fall_val.shape[0]), np.zeros(adl_val.shape[0])))
    x_val, y_val = shuffle(x_val, y_val)

    # Train the model
    hist50 = model.fit(
        x_train,
        y_train,
        epochs=50,
        batch_size=batch_size,
        validation_data=(x_val, y_val),
        verbose=verbose,
    )
    log("Finished training first 50 epochs")

    # Copy model at current state and train for 50 more epochs
    model50 = tf.keras.models.clone_model(model)
    model50.set_weights(model.get_weights())
    log("Cloned model")

    hist100 = model.fit(
        x_train,
        y_train,
        epochs=100,
        batch_size=batch_size,
        validation_data=(x_val, y_val),
        verbose=verbose,
        initial_epoch=50,
    )
    log("Finished training last 50 epochs")

    total_hist = {key: hist50.history[key] + hist100.history[key] for key in hist50.history.keys()}

    return model50, model, total_hist