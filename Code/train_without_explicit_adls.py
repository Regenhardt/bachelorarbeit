import os
import model
import numpy as np
from video_processor import VideoProcessor
from KUL_data import ScenarioData, get_metadata
import tensorflow as tf
from preprocessor import preprocess_scenario
import sys
from training_config import (
    default_model_dir,
    epochs_list,
    units_list_list,
    filter_list_list,
    model_format,
)

model_description = "without_expl_adls"

if "--help" in sys.argv or "-h" in sys.argv:
    print(
        """
Usage: python train_without_explicit_adls.py [options]

Options:
    -h, --help: Show this help message
    --local: Use local data instead of environment variables
"""
    )
    sys.exit(0)

# First, make sure SAVE_DIR and VID_DIR environment variables are set, unless "local" was passed as an argument
if not "--local" in sys.argv:
    if os.environ.get("SAVE_DIR") is None:
        raise Exception("SAVE_DIR environment variable is not set")
    if os.environ.get("VID_DIR") is None:
        raise Exception("VID_DIR environment variable is not set")

# Get save directory from environment variable
save_dir = os.environ.get("SAVE_DIR")

if save_dir is None:
    save_dir = default_model_dir

video_processor = VideoProcessor(model.num_frames, model.capture_window)

fall_videos_metadata: list[ScenarioData] = get_metadata()

# One training for epochsxunits
for epochs in epochs_list:
    for units_list in units_list_list:
        for filter_list in filter_list_list:
            model_name = "{}_{}_{}e".format(
                model.get_model_structure_name(filter_list, units_list),
                model_description,
                epochs,
            )

            model_file = os.path.join(save_dir, model_name + "." + model_format)

            # If the model file already exists, skip this training round
            if os.path.exists(model_file) and not "--force" in sys.argv:
                print(
                    "[train_without_explicit_adls] Model file already exists: {}".format(
                        model_file
                    )
                )
                continue

            fall_data = np.empty((0, model.num_frames, model.num_features))
            fall_adl_data = np.empty((0, model.num_frames, model.num_features))
            # Process all scenarios
            for scenario in fall_videos_metadata:
                new_fall_data, new_fall_adl_data = preprocess_scenario(
                    scenario, random_shift_fall_data=5
                )

                # Gather data
                if fall_data.shape[0] > 0 and len(fall_data.shape) == len(
                    new_fall_data.shape
                ):
                    fall_data = np.concatenate((fall_data, new_fall_data))
                else:
                    print("No fall data for scenario {}".format(scenario.Scenario))
                if fall_adl_data.shape[0] > 0 and len(fall_adl_data.shape) == len(
                    new_fall_adl_data.shape
                ):
                    fall_adl_data = np.concatenate((fall_adl_data, new_fall_adl_data))
                else:
                    print("No fall adl data for scenario {}".format(scenario.Scenario))

            # Train the model
            if fall_data.shape[0] > 0 and fall_adl_data.shape[0] > 0:
                trained_model = model.train_model(
                    fall_data,
                    fall_adl_data,
                    epochs=epochs,
                    units_list=units_list,
                    filters_list=filter_list,
                )
            else:
                print("[train_without_explicit_adls] No data at all")

            trained_model.save(model_file)
            print("[train_without_explicit_adls] Saved model to {}".format(model_file))
