import os
import model
import numpy as np
from video_processor import VideoProcessor
from KUL_data import ScenarioData, get_metadata, get_adl_metadata
import tensorflow as tf
from preprocessor import preprocess_scenario, preprocess_adl_video
import sys
from training_config import (
    default_model_dir,
    epochs_list,
    units_list_list,
    filter_list_list,
    model_format,
)

model_description = "only_adls"

if "--help" in sys.argv or "-h" in sys.argv:
    print(
        """
Usage: python train_with_explicit_adls.py [options]

Options:
    -h, --help: Show this help message
    --local: Use local data instead of environment variables
"""
    )
    sys.exit(0)

# First, make sure SAVE_DIR and VID_DIR environment variables are set, unless "local" was passed as an argument
if not "--local" in sys.argv:
    if os.environ.get("SAVE_DIR") is None:
        raise Exception("SAVE_DIR environment variable is not set")
    if os.environ.get("VID_DIR") is None:
        raise Exception("VID_DIR environment variable is not set")

# Get save directory from environment variable
save_dir = os.environ.get("SAVE_DIR")

if save_dir is None:
    save_dir = default_model_dir

video_processor = VideoProcessor(model.num_frames, model.capture_window)

fall_videos_metadata: list[ScenarioData] = get_metadata()
adl_videos_metadata: list[str] = get_adl_metadata()

for epochs in epochs_list:
    for units_list in units_list_list:
        for filter_list in filter_list_list:
            model_name = "{}_{}_{}e".format(
                model.get_model_structure_name(filter_list, units_list),
                model_description,
                epochs,
            )

            model_file = os.path.join(save_dir, model_name + "." + model_format)

            # If the model file already exists, skip this training round
            if os.path.exists(model_file) and not "--force" in sys.argv:
                print(
                    "[train_only_adls] Model file already exists: {}".format(model_file)
                )
                continue

            # Gather data for all scenarios
            adl_data: np.ndarray = None
            for scenario in fall_videos_metadata:
                _, sc_fall_adl_data = preprocess_scenario(scenario)
                adl_data = (
                    sc_fall_adl_data
                    if adl_data is None
                    else np.concatenate((adl_data, sc_fall_adl_data))
                )

            # Add explicit ADL data
            for video_path in adl_videos_metadata:
                explicit_adl_data = preprocess_adl_video(video_path)
                if (
                    explicit_adl_data is not None
                    and len(explicit_adl_data.shape) > 0
                    and explicit_adl_data.shape[0] > 0
                ):
                    adl_data = (
                        explicit_adl_data
                        if adl_data is None
                        else np.concatenate((adl_data, explicit_adl_data))
                    )

            # Train the model
            if adl_data.shape[0] > 0:
                trained_model = model.train_model(
                    np.empty((0, *adl_data.shape[1:])),
                    adl_data,
                    epochs=epochs,
                    units_list=units_list,
                    filters_list=filter_list,
                )
            else:
                print("[train_only_adls] No data at all")

            trained_model.save(model_file)
            print("[train_only_adls] Saved model to {}".format(model_file))
