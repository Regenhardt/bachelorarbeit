import cv2
import tkinter as tk
import os
import numpy as np
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

from pose_processor import PoseProcessor

# Function to get the position of a TKinter window
def get_window_position(root):
    root.update_idletasks()
    return root.winfo_x(), root.winfo_y()

# Process a given feed
def run_feed(pose_processor: PoseProcessor, cap: cv2.VideoCapture):
    paused = False  # Flag to indicate if the video is paused or not
    # Timestamp to keep track of the time elapsed since the last frame was processed and add an fps counter to the image
    prev_time = cv2.getTickCount()
    img = None
    processed_img = None
    fitted_bg = None
    while True:
        if not paused:
            ret, img = cap.read()
            if not ret:
                break
            processed_img, fitted_bg, detected = pose_processor.process_and_draw(img.copy())
            curr_time = cv2.getTickCount()
            fps = cv2.getTickFrequency() / (curr_time - prev_time)
            prev_time = curr_time
            #cv2.putText(processed_img, f"FPS: {int(fps)}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            #cv2.putText(processed_img, f"Detected: {detected}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            #cv2.imshow('Original image', img)
            cv2.imshow('Overlayed image', processed_img)
            # cv2.imshow('Normalized World Pose', normalized_bg)
            #cv2.imshow('Fitted Pose', fitted_bg)
            # cv2.imshow('Raw World Pose', raw_bg)
            key = cv2.waitKey(1) & 0xFF  # Short wait time when video is playing

            current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
            if current_frame >= 2980 and pose_processor.has_full_sequence():
                try:
                    save_sequence(pose_processor, cap)
                except Exception as e:
                    print(f"Error while saving sequence: {e}")
                    print(f"Current frame: {current_frame}")
                return
        else:
            key = cv2.waitKey(100) & 0xFF  # Longer wait time when video is paused

        if key == ord('q'):
            break
        elif key == 32:  # ASCII for spacebar
            paused = not paused  # Toggle the paused state
        elif key == ord('s'):
            # Save the current frame
            base_name = "current_frame"
            ext = ".png"
            i = 0
            while os.path.exists(f"{base_name}_{i}{ext}"):
                i += 1
            cv2.imwrite(f"{base_name}_{i}{ext}", img)
        elif key == ord('b'):
            save_sequence(pose_processor, cap)

def save_sequence(pose_processor: PoseProcessor, cap: cv2.VideoCapture):
    """
    Run 45 frames back. 
    Then run them through the processor and save each 
        - 1. original image, 
        - 2. overlayed image, 
        - 3. pose of the frame, overlayed on the original image
        - 4. pose of the frame without the image
    Then save the sequence of poses in a numpy file  
    """
    # Get the current frame number
    frame_number = cap.get(cv2.CAP_PROP_POS_FRAMES)
    print(f"Current frame number: {frame_number}")
    if frame_number < 45*2:
        print("Not enough frames to go back")
        return
    # Go back 45*2 frames
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_number - 45*2)
    # Clear the recent poses
    pose_processor.recent_poses.clear()
    # Process the next 45*2 frames
    original_images = []
    overlayed_poses = []
    overlayed_coordinates = []
    poses = []
    print("Processing frames")
    current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
    while len(poses) < 45:
        ret, img = cap.read()
        if not ret:
            print("Error while running previous frames")
            break
        # if len(pose_processor.recent_poses) == 0:
        #     print("No recent poses detected")
        #     break
        print(f"Processing frame {current_frame}")
        processed_img, fitted_bg, detected = pose_processor.process_and_draw(img.copy(), True)
        print(f"Number of recent poses after processing: {len(pose_processor.recent_poses)}")
        if current_frame % 2 == 1 or not detected:
            print("Skipping frame")
            current_frame = current_frame + 1
            continue
        last_pose = pose_processor.recent_poses[-1]
        last_coordinates = utils.get_points_from_mediapipe_pose(last_pose)
        overlayed_coordinate = img.copy()
        overlayed_coordinate = utils.draw_pose_on_image(overlayed_coordinate, last_coordinates)
        clear_img = np.ones_like(img) * 255
        clear_img = utils.draw_pose_on_image(clear_img, last_coordinates)
        # 1.
        original_images.append(img.copy())
        # 2.
        overlayed_poses.append(processed_img)
        # 3.
        overlayed_coordinates.append(overlayed_coordinate)
        # 4.
        poses.append(clear_img)
        current_frame = current_frame + 1
    print("Frames processed")
    # Save the images and poses
    save_dir = "last_sequence_annotated"
    os.makedirs(save_dir, exist_ok=True)
    orig_dir = os.path.join(save_dir, "original_images")
    overlayed_poses_dir = os.path.join(save_dir, "overlayed_poses")
    overlayed_coordinates_dir = os.path.join(save_dir, "overlayed_coordinates")
    poses_dir = os.path.join(save_dir, "poses")
    os.makedirs(orig_dir, exist_ok=True)
    os.makedirs(overlayed_poses_dir, exist_ok=True)
    os.makedirs(overlayed_coordinates_dir, exist_ok=True)
    os.makedirs(poses_dir, exist_ok=True)
    
    if len(original_images) < 45:
        print("Insufficient frames processed")
        return

    print("Directory structure ok, saving images")
    for i in range(45):
        cv2.imwrite(os.path.join(orig_dir, f"{i}.png"), original_images[i])
        cv2.imwrite(os.path.join(overlayed_poses_dir, f"{i}.png"), overlayed_poses[i])
        cv2.imwrite(os.path.join(overlayed_coordinates_dir, f"{i}.png"), overlayed_coordinates[i])
        cv2.imwrite(os.path.join(poses_dir, f"{i}.png"), poses[i])
    # Save the whole sequence in save_dir
    print("Saving sequence")
    seq = utils.get_point_seq_from_mediapipe_poses(pose_processor.recent_poses)
    if os.path.exists(os.path.join(save_dir, 'seq.npy')):
        os.remove(os.path.join(save_dir, 'seq.npy'))
    np.save(os.path.join(save_dir, 'seq.npy'), seq)



# Initialization
processor: PoseProcessor = PoseProcessor(frames=45, skip_frames=1)

def webcam_source(pose_processor: PoseProcessor, webcam_id: int = 0):
    cap: cv2.VideoCapture = cv2.VideoCapture(webcam_id)
    run_feed(pose_processor, cap)
    cap.release()
    cv2.destroyAllWindows()

def video_source(pose_processor: PoseProcessor, video_path: str, start_at_second: int = 0):
    if not os.path.exists(video_path):
        print(f"Video file {video_path} does not exist.")
        return
    cap = cv2.VideoCapture(video_path)
    cap.set(cv2.CAP_PROP_POS_MSEC, start_at_second * 1000)
    run_feed(pose_processor, cap)
    cap.release()
    cv2.destroyAllWindows()


# Create a simple TKinter window to get user's preferred screen
root = tk.Tk()
root.title("Place me on your desired screen and press OK")
root.geometry("400x200")
button = tk.Button(root, text="OK", command=root.quit)
button.pack(pady=50)
root.mainloop()

# Get the position of the TKinter window
x_offset, y_offset = get_window_position(root)
root.destroy()


# Window positions
window_size: tuple[int, int] = (640, 480)
#cv2.namedWindow('Original image', cv2.WINDOW_NORMAL)
#cv2.resizeWindow('Original image', *window_size)

cv2.namedWindow('Overlayed image', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Overlayed image', *window_size)

# cv2.namedWindow('Normalized World Pose', cv2.WINDOW_NORMAL)
# cv2.resizeWindow('Normalized World Pose', *window_size)

#cv2.namedWindow('Fitted Pose', cv2.WINDOW_NORMAL)
#cv2.resizeWindow('Fitted Pose', *window_size)

# cv2.namedWindow('Raw World Pose', cv2.WINDOW_NORMAL)
# cv2.resizeWindow('Raw World Pose', *window_size)

#cv2.moveWindow('Original image', x_offset, y_offset)
cv2.moveWindow('Overlayed image', x_offset + window_size[0], y_offset)
#cv2.moveWindow('Fitted Pose', x_offset + window_size[0], y_offset)
# cv2.moveWindow('Raw World Pose', x_offset + window_size[0], y_offset + window_size[1])

# Main loop
#webcam_source(processor, 0)
video_source(processor, os.path.join("Data", "KUL", "Fall8_Cam1.avi"), 0) # "../../Data/KUL/Fall2_Cam4.avi"
cv2.destroyAllWindows()
