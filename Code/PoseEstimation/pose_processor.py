from typing import Tuple, List, Optional
import cv2
import mediapipe as mp
import numpy as np
from mediapipe.framework.formats import landmark_pb2

Landmark = tuple[float, float, float] # (x, y, z) although z isn't used (yet)
Pose = list[Landmark]

class PoseProcessor:
    """
    A class used to process images and detect poses using the mediapipe library.

    Attributes:
        _mp_pose: A `mediapipe.solutions.pose` object used to detect poses.
        _pose: A `mediapipe.solutions.pose.Pose` object used to detect poses.
        recent_poses: A list of the most recent poses detected.
        recent_world_poses: A list of the most recent poses detected in the world coordinate system.
        frame_count: The number of frames to keep track of in the recent poses lists.
        skip_frames: The number of frames to skip between each frame stored in `recent_poses`.
        frames_skipped: The number of frames skipped since the last frame was stored in `recent_poses`.
    """
    def __init__(self, frames: int = 90, skip_frames: int = 1) -> None:
        self.recent_poses: List[landmark_pb2.NormalizedLandmarkList] = []
        self.recent_world_poses: List[landmark_pb2.LandmarkList] = []
        self.frame_count: int = frames
        self.skip_frames: int = skip_frames
        self.frames_skipped: int = 0
        self._no_pose_found = False
        self._mp_pose: mp.solutions.pose = mp.solutions.pose
        self._pose: mp.solutions.pose.Pose = self._mp_pose.Pose()

    def process(self, img_bgr):
        """
        Processes the image using the mediapipe library and returns the results.
        Keeps track of a number of recent poses, skipping frames as specified in the constructor.
        """

        if self.frames_skipped < self.skip_frames:
            self.frames_skipped += 1
            return None
        else:
            self.frames_skipped = 0


        img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
        results = self._pose.process(img_rgb)

        # Store the latest pose landmarks and remove the oldest if more than requested are stored
        if results.pose_landmarks:
            self.recent_poses.append(results.pose_landmarks)
            self.recent_world_poses.append(results.pose_world_landmarks)
            if len(self.recent_poses) > self.frame_count:
                self.recent_poses.pop(0)
                self.recent_world_poses.pop(0)
            self._no_pose_found = False
        elif self._no_pose_found:
            self.recent_poses = []
            self.recent_world_poses = []
            self.frames_skipped = 0
            self._no_pose_found = False
        else:
            self._no_pose_found = True

        return results
    
    def has_full_sequence(self) -> bool:
        """
        Checks if the number of poses stored is equal to the number of frames required.
        """
        return len(self.recent_poses) == self.frame_count

    def is_human(self, landmarks, confidence_threshold=0.5, movement_threshold=0.005) -> bool:
        """
        Determine if a set of landmarks likely represents a human.

        Args:
        - landmarks: The pose landmarks to be checked.
        - confidence_threshold: The minimum confidence score required for each landmark.
        - movement_threshold: The minimum movement required between poses to consider them different.

        Returns:
        - Boolean indicating if the landmarks likely represent a human.
        """

        # Check if confidence scores of key landmarks are above the threshold
        # for landmark in landmarks.landmark:
        #     if landmark.visibility < confidence_threshold:
        #         return False

        # Compare the current landmarks with those in the recent poses to check for movement
        for recent_landmarks in self.recent_poses:
            if all(
                abs(landmark.x - recent_landmark.x) < movement_threshold
                and abs(landmark.y - recent_landmark.y) < movement_threshold
                and abs(landmark.z - recent_landmark.z) < movement_threshold
                for landmark, recent_landmark in zip(
                    landmarks.landmark, recent_landmarks.landmark
                )
            ):
                # The current pose is too similar to a recent pose, indicating a potential false positive
                return False

        # If all checks pass, it's likely a human
        return True

    def draw_connections(
        self,
        bg,
        landmarks,
        scale_factor=1,
        x_trans=0,
        y_trans=0,
        x_offset=0,
        y_offset=0,
    ):
        """Draws the pose connections on the image using the mediapipe library."""
        for connection in self._mp_pose.POSE_CONNECTIONS:
            start_idx = connection[0]
            end_idx = connection[1]
            start_point = (
                int(
                    (landmarks.landmark[start_idx].x - x_offset) * scale_factor
                    + x_trans
                ),
                int(
                    (landmarks.landmark[start_idx].y - y_offset) * scale_factor
                    + y_trans
                ),
            )
            end_point = (
                int(
                    (landmarks.landmark[end_idx].x - x_offset) * scale_factor + x_trans
                ),
                int(
                    (landmarks.landmark[end_idx].y - y_offset) * scale_factor + y_trans
                ),
            )
            cv2.line(bg, start_point, end_point, (0, 255, 0), 2)

    def draw_image_overlay(self, img, landmarks):
        """Draws the pose landmarks on the image using the mediapipe library."""

        mp.solutions.drawing_utils.draw_landmarks(
            img, landmarks, self._mp_pose.POSE_CONNECTIONS
        )
        return img

    def draw_normalized_world_pose(self, h, w, world_landmarks):
        """
        Draws the pose landmarks in the world coordinate system, with origin at the center of the image.

        Args:
            h: Height of the image.
            w: Width of the image.
            world_landmarks: A `PoseLandmark` object containing the pose landmarks to be drawn.

        Returns:
            bg: A `numpy` array with the pose drawn on it.
        """

        bg = np.zeros((h, w, 3), dtype=np.uint8)
        self.draw_connections(
            bg,
            world_landmarks,
            scale_factor=1,
            x_trans=0,
            y_trans=0,
            x_offset=0.5,
            y_offset=0.5,
        )
        for landmark in world_landmarks.landmark:
            x = int((landmark.x + 0.5) * w)
            y = int((landmark.y + 0.5) * h)
            cv2.circle(bg, (x, y), 5, (0, 255, 0), -1)
        return bg

    def get_normalized_poses(self) -> np.array:
        """
        Normalizes the motion trajectory of the recent poses to fit within a [0, 1] range for x, y, and z coordinates.
        
        Returns:
        - A list of lists, each sublist containing tuples of normalized x, y, and z coordinates for one pose.
        """
        if len(self.recent_poses) == 0:
            return []

        # Gather all landmarks from the last poses
        all_landmarks = np.array(
            [
                (landmark.x, landmark.y, landmark.z)
                for pose in self.recent_poses
                for landmark in pose.landmark
            ]
        )

        # Determine the min and max coordinates from all landmarks
        mins = np.min(all_landmarks, axis=0)
        maxs = np.max(all_landmarks, axis=0)

        # Calculate scaling factor and translation values for normalization
        ranges = maxs - mins
        scale_factor = 1 / np.max(ranges)
        translations = (1 - ranges * scale_factor) / 2

        # same but using numpy instead of list of list of tuples
        numpy_poses = np.array(
            [
                [
                    ((landmark.x - mins[0]) * scale_factor + translations[0],
                     (landmark.y - mins[1]) * scale_factor + translations[1],
                     (landmark.z - mins[2] * scale_factor + translations[2]))
                    for landmark in pose.landmark
                ]
                for pose in self.recent_poses
            ]
        )

        return numpy_poses


    def draw_normalized_motion(self, h, w):
        """
        Draws the normalized motion trajectory onto an image.
        
        Args:
        - h: Height of the image.
        - w: Width of the image.

        Returns:
        - A NumPy array representing the image with the drawn motion trajectory.
        """
        bg = np.zeros((h, w, 3), dtype=np.uint8)
        normalized_poses = self.get_normalized_poses()

        # Scale normalized landmarks x and y to image dimensions and draw them
        for pose in normalized_poses:
            for landmark in pose:
                x = int(landmark[0] * w)
                y = int(landmark[1] * h)
                cv2.circle(bg, (x, y), 5, (0, 255, 0), -1)

        return bg


    def draw_raw_landmarks(self, h, w, landmarks):
        """
        Draws the raw landmarks onto a black background,
        with origin at the top left of the image as provided by the mediapipe library.
        """
        bg = np.zeros((h, w, 3), dtype=np.uint8)
        self.draw_connections(bg, landmarks, scale_factor=w, x_trans=0, y_trans=0)
        for landmark in landmarks.landmark:
            x = int(landmark.x * w)
            y = int(landmark.y * h)
            cv2.circle(bg, (x, y), 5, (0, 255, 0), -1)
        return bg

    def process_and_draw(self, img, single_pose = False) -> tuple[np.ndarray, np.ndarray, bool]:
        """
        Processes the image, draws the detected pose landmarks on it and returns multiple images with the landmarks drawn on them.

        Args:
            img: A `numpy` array containing the image to be processed. It should be in BGR format, as returned by a cv2 cap.

        Returns:
            img: The original image with the pose landmarks and connections (i.e. limbs) drawn on it.
            fitted_bg: A `numpy` array with the pose landmarks drawn on black background,
                with all landmarks of all recent poses scaled and translated to fit the image.
            detected: A boolean indicating if a human was detected in the image.
            
        """
        results = self.process(img)

        # Draw the recent poses on the image
        if not single_pose or len(self.recent_poses) == 0:
            for landmarks in self.recent_poses:
                img = self.draw_image_overlay(img, landmarks)
        else:
            img = self.draw_image_overlay(img, self.recent_poses[-1])

        h, w, _ = img.shape

        fitted_bg = self.draw_normalized_motion(h, w)
             
        return img, fitted_bg, self.recent_poses != []
    
    def process_and_get_current_sequence(self, img) -> Optional[List[List[Tuple[float, float, float]]]]:
        """
        Processes the image and returns the current motion sequence.

        Args:
            img: A `numpy` array containing the image to be processed.

        Returns:
            A list of lists, each sublist containing tuples of normalized x, y, and z coordinates for one pose.
            Or None if no pose was detected.
            If a frame was skipped, the previous sequence is returned (if it exists at all).
        """
        results = self.process(img)

        return self.get_normalized_poses() if self.has_full_sequence() else None
