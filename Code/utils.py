import datetime
from typing import List, NamedTuple
import numpy as np
from mediapipe.framework.formats import landmark_pb2
import cv2
import os


def timestamp() -> str:
    return datetime.datetime.now().strftime("[%H:%M:%S:%f]")[:-4]+"]"

def get_points_from_mediapipe_pose(pose: landmark_pb2.NormalizedLandmarkList) -> np.ndarray:
    return np.array(
        [
            (landmark.x, landmark.y, landmark.z)
            for landmark in pose.landmark
        ]
    )

def get_point_seq_from_mediapipe_poses(poses: list) -> np.ndarray:
    return np.array(
        [
            get_points_from_mediapipe_pose(pose)
            for pose in poses
        ]
    )

def draw_pose_on_image(img: np.array, pose_landmarks: np.array, color: tuple = (0, 255, 0)) -> np.array:
    """Draws a pose on an image"""
    w, h = img.shape[1], img.shape[0]
    for landmark in pose_landmarks:
        x = int(landmark[0] * w)
        y = int(landmark[1] * h)
        cv2.circle(img, (x, y), 5, color, -1)

    return img

def draw_sequence_on_image(img: np.array, sequence: np.array, color: tuple=(0, 255, 0)):
    """Draws a sequence of poses on an image"""
    for pose_landmarks in sequence:
        img = draw_pose_on_image(img, pose_landmarks, color)

    return img

def shade_color(color, scalar):
    # Ensure scalar is between 0 and 1 to avoid invalid color values
    scalar = max(0, min(scalar, 1))
    return tuple(int(component * scalar) for component in color)

def draw_key_poses_on_image(img, sequence, color=(255, 255, 255)):
    """Draws the first, a later and the last pose of a sequence on an image"""
    if len(sequence) > 0:
        img = draw_pose_on_image(img, sequence[0], shade_color(color, 0.8))
        img = draw_pose_on_image(img, sequence[-(len(sequence)//8)], shade_color(color, 0.6))
        #img = draw_pose_on_image(img, sequence[-(len(sequence)//8)], shade_color(color, 0.4))
        
        img = draw_pose_on_image(img, sequence[-1], shade_color(color, 0.0))

    return img

def convert_to_positive_indices(seq, indices):
    """
    Convert negative indices to positive indices based on the length of the sequence.

    Args:
    seq (list): The sequence from which indices are derived.
    indices (list): List of indices (can be both positive and negative).

    Returns:
    list: List of converted positive indices.
    """
    length = len(seq)
    positive_indices = [(index if index >= 0 else length + index) for index in indices]
    return positive_indices

def file_exists(file_path: str) -> bool:
    return os.path.exists(file_path) and os.path.isfile(file_path) and os.path.getsize(file_path) > 0

class ModelConfig(NamedTuple):
    filters: List[int]
    units: List[int]

class InferenceTime(NamedTuple):
    model_inference_time: float
    mediapipe_inference_time: float
    measurement_points: int
    measurement_time: float

def print_inference_time(inference_time: InferenceTime) -> str:
    time_ms = round(inference_time.model_inference_time * 1000)
    mediapipe_time_ms = round(inference_time.mediapipe_inference_time * 1000)
    print(f"Measured over {round(inference_time.measurement_time)}s and {inference_time.measurement_points} data points:\n"
            f"Inference model: {time_ms}ms, "
            f"Inference MediaPipe: {mediapipe_time_ms}ms\n")
    
def typst_table(headers: list[str], data: list[list], caption: str, *args: str) -> str:
    """Create a typst table from the given headers and data
    
    Args:
    headers (list[str]): A list of headers for the table.
    data (list[list]): A list of lists, where each inner list represents a row of data.
    caption (str): The caption for the table.
    *args (str): Additional arguments for the table.
    
    Returns:
    str: The typst table string.

        Example output:
#figure(
  table(
    columns: 3,
    **kwargs,
    [Modell], [Parameteranzahl], [Sensitivität in %],
    
    [3c_128f_64f_32f_2l_10u_5u], [178782], [98,95],
    [2c_16f_8f_2l_100u_50u], [283171], [92,96],
    [3c_128f_64f_32f_2l_100u_50u], [1008987], [85,86],
  ),
  caption: [Sensitivität der großen Modelle nach 50 Epochen]
)
    """
    # Check if the number of headers matches the number of columns in the data
    num_columns = len(headers)
    for row in data:
        if len(row) != num_columns:
            raise ValueError("Number of headers does not match the number of columns in the data.")

    # Convert headers and data into typst format
    headers_str = ", ".join(f"[{header}]" for header in headers)
    data_str = ",\n    ".join(", ".join(f"[{str(item)}]" for item in row) for row in data)
    # Format the final typst table string
    table_str = (
        f"#figure(\n"
        f"  table(\n"
    )
    table_str += (
        f"    columns: {num_columns},\n"
    )
    if args:
        for arg in args:
            table_str += (
                f"    {arg},\n"
            )
    table_str += (
        f"    {headers_str},\n\n"
    )
    table_str += (
        f"    {data_str},\n"
        f"  ),\n"
    )
    table_str += (
        f"  caption: [{caption}]\n"
        f")"
    )

    return table_str